import React from 'react'
import './filter.scss'

export default function Filter(props){
    const { selected, handleChange, values } = props
        return(
            <select value = {selected} 
                    onChange = {handleChange}
            >

                {values.map((category,index) => {
                    return  <option 
                                value = {category} 
                                key   = {category + index}>
                                {category}
                            </option>
                })}
            </select>
            )
}

