import React, {Component} from 'react'
import './card.scss'
import PropTypes from 'prop-types';


class Card extends Component{
    constructor(props){
        super(props)
        this.state = {
        }
    }


    render(){
        const { content, onDelete, updateLikes } = this.props
        const { 
            title = "", 
            category = "", 
            likes = "", 
            dislikes = "",
            id = "" ,
        } = content
        return(
            <div className = "custom-card">
                <p className="italic">{category}</p>
                <div className = "inline">
                    <Likes dislike = {false} nb = {likes} />
                    <Likes dislike = {true} nb  = {dislikes} />
                </div>
                <CustomButton 
                    handleClick = {onDelete} 
                    value = {id} 
                    content = "Delete"/>
                <ToggleButton 
                    id = {id}
                    upDateLikes = {updateLikes}
                />
                <h4>{title}</h4>
            </div>
        )
    }
}
export default Card

Card.propTypes = {
    content: PropTypes.object,
    onDelete: PropTypes.func,
    upDateLikes: PropTypes.func
}

class ToggleButton extends Component{
    constructor(props){
        super(props)
        this.state = {
            like:false
        }
    }
    handleClick = () => {
        const { upDateLikes, id } = this.props
        const { like } = this.state

        this.setState((prevState) => {
            return {
                like:!prevState.like
            }
        },() => upDateLikes(like,id))
        
    }

    render(){
        var content = this.state.like ? "Unlike" : "I like it"
        return(
            <button 
                className = "toggleButton"
                onClick = {this.handleClick} 
                value   = {this.props.id}>
                    {content}
            </button>
        ) 
    }    
}

ToggleButton.propTypes = {
    id:PropTypes.string,
    updateLikes:PropTypes.func
}

function Likes (props){
    const { dislike, nb } = props
    const style = dislike ? "thumb dislikes" : "thumb"
    return  <p className = {style}>{nb}</p>
}

Likes.propTypes = {
    dislike:PropTypes.bool,
    nb:PropTypes.number
}

function CustomButton(props){
    return (
        <button onClick = {props.handleClick} 
                value = {props.value}
                className = "custom-btn"
        >
                {props.content}
        </button>
    )
}

CustomButton.propTypes = {
    handleClick: PropTypes.func,
    value:PropTypes.string,
    content: PropTypes.string
}