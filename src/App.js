import React, { Component } from 'react';
import movies$ from './data/movies'
import './App.css';
import Card from './components/card'
import Filter from './components/filter'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      selected:"All",
      displayNb: 8,
    }
  }

  componentDidMount(){
    movies$.then((value) => this.setState({movies:value}))
  }

  handleDelete = (id) => {
    const movieId = parseInt(id.target.value)
    //console.log(parseInt(id.target.value))
    const newState = this.state.movies.filter(movie => parseInt(movie.id) !== movieId)
    this.setState({movies:newState},()=>console.log(this.state.movies))
  }

  handleLikes = (likeStatus,id) => {
    const newState = this.state.movies.map((movie,index) => {
      if((movie.id === id) && likeStatus === true) {
        return {
          ...movie,
          likes: movie.likes - 1,
        }
      }
      else if((movie.id === id) && likeStatus === false){
        return {
          ...movie,
          likes: movie.likes + 1,
        }
      }
      else{
        return movie
      }
    })
    this.setState({movies:newState})
  }

  handleChange = (event) => {
    this.setState({selected:event.target.value})
  }
  handleDisplay = (event) => {
    this.setState({displayNb:event.target.value})
  }

  getValues(object,key){
    let categories = ["All"]
    if(object)
      object.forEach((obj) => {
        if(!categories.includes(obj[key])){
          categories.push(obj[key])
        }
      }) 
    
    return categories
  }

  render() {
    const { movies, selected, displayNb } = this.state
    let categories = this.getValues(movies,"category")

    return (
     <div className = "card-container">
       <div style = {{width:"100%"}}>
        {this.state.movies && 
        <Filter 
            values = {categories}
            selected = {selected}
            handleChange = {this.handleChange}
          />} 
          <Filter 
            values = {[4,8,12]}
            selected = {displayNb}
            handleChange = {this.handleDisplay}/>
       </div>

      {movies && movies.map((movie,index) => 
        (( selected === movie.category 
        || selected === "All")
        && index < displayNb) 
        && <Card 
          key = {index+movie}
          content = {movies[index]}
          onDelete = {this.handleDelete}
          updateLikes = {this.handleLikes}
        />
      )}
     </div>
    );
  }
}

export default App;
